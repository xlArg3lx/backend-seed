# Proyecto Java con Spring Boot - Backend SEED

Este proyecto es una aplicación backend construida en Java utilizando el framework Spring Boot. La aplicación proporciona funcionalidades CRUD para usuarios, autenticación con tokens JWT (JSON Web Token), gestión de roles y permisos, así como operaciones CRUD para menús.

## Características

- **CRUD de Usuarios:** La aplicación permite realizar operaciones CRUD (Crear, Leer, Actualizar, Eliminar) en la entidad de usuarios.

- **Autenticación y Autorización:** Implementa un sistema de autenticación basado en tokens JWT. Además, gestiona roles y permisos para controlar el acceso a diferentes recursos.

- **Refresh Token:** La aplicación utiliza un mecanismo de refresh token para mantener las sesiones activas.

- **CRUD de Menús:** Proporciona operaciones CRUD para gestionar menús en la aplicación.

## Configuración del Proyecto

### Requisitos Previos

- JDK 21
- Maven
- IDE compatible con Spring Boot (por ejemplo, IntelliJ, Eclipse)

### Configuración de Git Hooks

Este proyecto utiliza ganchos personalizados para garantizar prácticas de desarrollo consistentes. Asegúrate de ejecutar el siguiente comando para configurar la ubicación de los ganchos:

```bash
git config core.hooksPath .\hooks\
```

## Generación de Documentación Swagger

Para generar la documentación Swagger, sigue estos pasos:

1. Instala el CLI de Swagger de manera global usando npm:

   ```bash
   npm install -g swagger-cli

2. Una vez instalado, navega hasta la ruta .src/main/resources en tu proyecto.
3. Ejecuta el siguiente comando para generar el archivo YAML de Swagger:

   ```bash
   swagger-cli bundle swagger-documentation.yml --outfile _build/openapi.yaml --type yaml

## Documentación Adicional

La documentación detallada de las API y los puntos finales está disponible en http://localhost:8080/swagger-ui/index.html Asegúrate de revisar la documentación para obtener información completa sobre el uso de la API.
