package co.net.tps.core.ani.domain.repository;

import co.net.tps.core.ani.domain.dto.AniDto;

import java.util.Optional;

public interface IAniRepository {
	Optional<AniDto> findById(Long dni);
}
