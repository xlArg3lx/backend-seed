package co.net.tps.core.ani.web.controller;

import co.net.tps.core.ani.domain.dto.AniDto;
import co.net.tps.core.ani.domain.service.IAniService;
import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.domain.enums.ActionLogEnum;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ani")
public class AniController {

    private static final String MODULE = "ANI";
    private final IAniService aniService;

    public AniController(IAniService aniService) {
        this.aniService = aniService;
    }

    @PreAuthorize("hasAuthority('ani:show')")
    @GetMapping("/{dni}")
    public ResponseEntity<AniDto> getByDni(@PathVariable("dni") Long dni, HttpServletRequest request) {
        AniDto response = aniService.findById(dni);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("READ")
                .message("Se realiza la consulta cédula " + dni)
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.ok(response);
    }

}
