package co.net.tps.core.ani.persistence;

import co.net.tps.core.ani.domain.dto.AniDto;
import co.net.tps.core.ani.domain.repository.IAniRepository;
import co.net.tps.core.ani.persistence.crudrepository.AniCrudRepository;
import co.net.tps.core.ani.persistence.mapper.AniMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class AniRepository implements IAniRepository {

    private final AniCrudRepository aniCrudRepository;
    private final AniMapper aniMapper;

    public AniRepository(AniCrudRepository aniCrudRepository, AniMapper aniMapper) {
        this.aniCrudRepository = aniCrudRepository;
        this.aniMapper = aniMapper;
    }

    @Override
    public Optional<AniDto> findById(Long dni) {
        return aniCrudRepository.findById(dni).map(aniMapper::toAniDto);
    }
}
