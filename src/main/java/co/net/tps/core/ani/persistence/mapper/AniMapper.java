package co.net.tps.core.ani.persistence.mapper;

import co.net.tps.core.ani.domain.dto.AniDto;
import co.net.tps.core.ani.persistence.entity.AniEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * 
 */
@Mapper(componentModel = "spring")
public interface AniMapper {

	@Mapping(source = "cedula", target = "dni")
	@Mapping(source = "nombre1", target = "firstName")
	@Mapping(source = "nombre2", target = "secondName")
	@Mapping(source = "apellido1", target = "firstSurname")
	@Mapping(source = "apellido2", target = "secondSurname")
	@Mapping(source = "particula", target = "particle")
	@Mapping(source = "vigencia", target = "validity")
	@Mapping(source = "fechaNacimiento", target = "birthdate")
	@Mapping(source = "fechaExpedicion", target = "expeditionDate")
	@Mapping(source = "letras", target = "letters")
	@Mapping(source = "fechaResolucion", target = "resolutionDate")
	@Mapping(source = "resolucion", target = "resolution")
	@Mapping(source = "genero", target = "gender")
	@Mapping(target = "remote", ignore = true)
	AniDto toAniDto(AniEntity aniEnity);
	
	@InheritInverseConfiguration
	AniEntity toAniEntity(AniDto dto);
	
}
