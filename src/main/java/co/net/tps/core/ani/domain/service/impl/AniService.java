package co.net.tps.core.ani.domain.service.impl;

import co.net.tps.core.ani.domain.dto.AniDto;
import co.net.tps.core.ani.domain.repository.IAniRepository;
import co.net.tps.core.ani.domain.service.IAniService;
import co.net.tps.core.exception.HttpBadRequestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Service
public class AniService implements IAniService {


    @Value("${ani.remote.url}")
    private String aniRemoteUrl;


    private final IAniRepository aniRepository;

    public AniService(IAniRepository aniRepository) {
        this.aniRepository = aniRepository;
    }

    @Override
    public AniDto findById(Long dni) throws HttpBadRequestException {
        if (dni.describeConstable().isEmpty())
            throw new HttpBadRequestException("Número de documento es obligatorio.");
        Optional<AniDto> optional = aniRepository.findById(dni);

        if (optional.isPresent()) {
            return optional.filter(e -> e.getValidity() != 21)
                    .orElseThrow(this::validity21);
        }

        return findAniCentral(dni);
    }

    private AniDto findAniCentral(Long dni) {
        try {
            RestTemplate restTemplate = new RestTemplate();
            String apiUrl = aniRemoteUrl + "/" + dni;
            ResponseEntity<AniDto> responseEntity = restTemplate.getForEntity(apiUrl, AniDto.class);

            if (!responseEntity.getStatusCode().is2xxSuccessful()) {
                throw this.validity21();
            }

            return responseEntity.getBody();
        } catch (RestClientException e) {
            throw this.validity21();
        }
    }

    private HttpBadRequestException validity21() {
        String message =
                "No ha sido posible la captura de la información del ciudadano en el sistema de " +
                "inscripción de candidatos, por presentar novedades con el estado de la vigencia " +
                "del documento de identidad o por que su cédula de primera vez se encuentra en " +
                "trámite por favor comuníquese con la mesa de ayuda a los teléfonos. " +
                "Por favor verifique la información ingresada.";
        return new HttpBadRequestException(message);
    }
}
