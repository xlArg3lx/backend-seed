package co.net.tps.core.ani.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "ani")
public class AniEntity {

    @Id
    private Long cedula;
    private String nombre1;
    private String nombre2;
    private String apellido1;
    private String apellido2;
    private String particula;
    private Byte vigencia;

    @Column(name = "fecha_nacim")
    private LocalDate fechaNacimiento;

    @Column(name = "fecha_exped")
    private LocalDate fechaExpedicion;

    private String letras;

    @Column(name = "fecha_resol")
    private LocalDate fechaResolucion;

    private String resolucion;
    private String genero;
}
