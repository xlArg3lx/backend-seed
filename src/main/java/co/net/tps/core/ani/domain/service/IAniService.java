package co.net.tps.core.ani.domain.service;

import co.net.tps.core.ani.domain.dto.AniDto;

public interface IAniService {

    AniDto findById(Long dni);
}
