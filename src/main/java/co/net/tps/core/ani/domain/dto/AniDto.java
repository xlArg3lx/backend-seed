package co.net.tps.core.ani.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class AniDto {

    private long dni;
    private String firstName;
    private String secondName;
    private String firstSurname;
    private String secondSurname;
    private String particle;
    private int validity;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate birthdate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate expeditionDate;

    private String letters;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate resolutionDate;

    private String resolution;
    private String gender;

    @JsonIgnore
    private boolean isRemote;
}
