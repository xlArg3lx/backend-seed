package co.net.tps.core.ani.persistence.crudrepository;

import co.net.tps.core.ani.persistence.entity.AniEntity;
import org.springframework.data.repository.CrudRepository;

public interface AniCrudRepository extends CrudRepository<AniEntity, Long> {
}
