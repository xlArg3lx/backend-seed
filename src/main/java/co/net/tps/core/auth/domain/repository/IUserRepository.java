package co.net.tps.core.auth.domain.repository;

import co.net.tps.core.auth.domain.dto.PermissionDto;
import co.net.tps.core.auth.domain.dto.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IUserRepository {
    Optional<UserDto> findTopByUsername(String username);

    void updateCurrentToken(String username, String token);
    void updatePassword(String username, String newPasswordEncode);

    Page<UserDto> findAll(Pageable pageable);
    Optional<UserDto> findById(int userId);
    UserDto save(UserDto userDto);

}
