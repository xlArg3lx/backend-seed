package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.PermissionDto;

import java.util.List;
import java.util.Optional;

public interface IPermissionService {
    List<PermissionDto> findAll();
    PermissionDto findById(int permissionId);
    PermissionDto save(PermissionDto permissionDto);
    void deletedById(int permissionId);
}
