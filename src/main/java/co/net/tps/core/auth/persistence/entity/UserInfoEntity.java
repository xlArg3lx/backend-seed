package co.net.tps.core.auth.persistence.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "auth_userinfo")
public class UserInfoEntity {
    @Id
    @Column(name = "user_id")
    private int userId;
    private String firstname;
    private String secondname;
    private String firstsurname;
    private String secondsurname;
    private String dni;
    private LocalDate dniexpdate;
    private LocalDate birthdate;
    private String email;
    private String celularphone;
    private String localphone;
}
