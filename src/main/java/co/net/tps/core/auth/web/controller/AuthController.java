package co.net.tps.core.auth.web.controller;

import co.net.tps.core.DefaultResponse;
import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.domain.dto.ChangePasswordDto;
import co.net.tps.core.auth.domain.dto.requestbody.AuthRefreshRequestBody;
import co.net.tps.core.auth.domain.dto.requestbody.AuthRequestBody;
import co.net.tps.core.auth.domain.dto.responsebody.AuthRefreshTokenResponseBody;
import co.net.tps.core.auth.domain.dto.responsebody.AuthResponseBody;
import co.net.tps.core.auth.domain.enums.ActionLogEnum;
import co.net.tps.core.auth.domain.service.IAuthService;
import co.net.tps.core.auth.domain.util.AuthUtil;
import co.net.tps.core.exception.HttpForbiddenException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {

    private static final String MODULE = "AUTH";
    private final IAuthService authService;
    private final AuthUtil authUtil;

    public AuthController(IAuthService authService, AuthUtil authUtil) {
        this.authService = authService;
        this.authUtil = authUtil;
    }
    
    @PostMapping(value = "/login")
    public ResponseEntity<AuthResponseBody> login(@RequestBody @Valid AuthRequestBody authRequestBody, HttpServletRequest request) {
        String ipRemote = authUtil.getTrueClientIp();
        AuthResponseBody authResponseBody = authService.login(authRequestBody, ipRemote);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("LOGIN")
                .message("Inicio de sesión para usuario: " + authRequestBody.getUsername())
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.ok(authResponseBody);
    }

    @PostMapping(value = "/refresh-token")
    public ResponseEntity<AuthRefreshTokenResponseBody> login(@RequestBody @Valid AuthRefreshRequestBody authRefreshRequestBody, HttpServletRequest request) {
        AuthRefreshTokenResponseBody authRefreshTokenResponseBody = authService.refreshToken(authRefreshRequestBody);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("REFRESH")
                .message("Refrescar token para usuario: " + authRefreshTokenResponseBody.getUsername())
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.ok(authRefreshTokenResponseBody);
    }

    @PostMapping(value = "/change-password")
    public ResponseEntity<DefaultResponse> login(
            @RequestBody @Valid ChangePasswordDto changePasswordDto,
            Authentication authentication, HttpServletRequest request) {

        String username = authentication.getName();
        authService.changePassword(changePasswordDto, username);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("LOGIN")
                .message("Cambio de contaseña: " + username)
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.ok(DefaultResponse.createOkMessage());
    }

    @PreAuthorize("hasAuthority('auth:basic')")
    @PostMapping(value = "/check")
    public ResponseEntity<Map<String, Object>> check(Authentication authentication) {
        Map<String, Object> response = Map.of(
                "status", "ok",
                "username", authentication.getName(),
                "details", authentication.getDetails(),
                "permissions", authentication.getAuthorities()
        );
        return ResponseEntity.ok(response);
    }
}
