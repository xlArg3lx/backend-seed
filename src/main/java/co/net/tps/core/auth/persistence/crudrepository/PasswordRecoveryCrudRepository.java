package co.net.tps.core.auth.persistence.crudrepository;

import co.net.tps.core.auth.persistence.entity.PasswordRecoveryEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PasswordRecoveryCrudRepository extends CrudRepository<PasswordRecoveryEntity, Integer> {
    Optional<PasswordRecoveryEntity> findTopByUserIdOrderByCreatedAtDesc(int userId);
}
