package co.net.tps.core.auth.persistence.crudrepository;

import co.net.tps.core.auth.persistence.entity.PermissionEntity;
import org.springframework.data.repository.CrudRepository;

public interface PermissionCrudRepository extends CrudRepository<PermissionEntity, Integer> {
}
