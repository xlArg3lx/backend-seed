package co.net.tps.core.auth.domain.service;

public interface IGoogleRecaptchaService {
    boolean validateToken(String token, String ipRemote);
}
