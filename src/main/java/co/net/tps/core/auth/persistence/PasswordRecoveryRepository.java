package co.net.tps.core.auth.persistence;

import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;
import co.net.tps.core.auth.domain.repository.IPasswordRecoveryRepository;
import co.net.tps.core.auth.persistence.crudrepository.PasswordRecoveryCrudRepository;
import co.net.tps.core.auth.persistence.entity.PasswordRecoveryEntity;
import co.net.tps.core.auth.persistence.mapper.PasswordRecoveryMapper;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class PasswordRecoveryRepository implements IPasswordRecoveryRepository {

	private final PasswordRecoveryCrudRepository passwordRecoveryCrudRepository;
	private final PasswordRecoveryMapper passwordRecoveryMapper;

	public PasswordRecoveryRepository(PasswordRecoveryCrudRepository passwordRecoveryCrudRepository, PasswordRecoveryMapper passwordRecoveryMapper) {
		this.passwordRecoveryCrudRepository = passwordRecoveryCrudRepository;
		this.passwordRecoveryMapper = passwordRecoveryMapper;
	}

	@Override
	public PasswordRecoveryDto save(PasswordRecoveryDto passwordRecoveryDto) {
		PasswordRecoveryEntity entity = passwordRecoveryMapper.toPasswordRecoveryEntity(passwordRecoveryDto);
		PasswordRecoveryEntity entitySaved = passwordRecoveryCrudRepository.save(entity);
		return passwordRecoveryMapper.toPasswordRecoveryDto(entitySaved);
	}

	@Override
	public Optional<PasswordRecoveryDto> findPasswordRecoveryByUserId(int userId) {
		Optional<PasswordRecoveryEntity> entity = passwordRecoveryCrudRepository.findTopByUserIdOrderByCreatedAtDesc(userId);
		return entity.map(passwordRecoveryMapper::toPasswordRecoveryDto);
	}
}
