package co.net.tps.core.auth.persistence.mapper;

import co.net.tps.core.auth.domain.dto.RoleDto;
import co.net.tps.core.auth.persistence.entity.RoleEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoleMapper {
    @Mapping(target = "permissions", ignore = true)
    RoleDto toRoleDto(RoleEntity roleEntity);

    RoleDto toRoleWithPermissionsDto(RoleEntity roleEntity);

    @Mapping(target = "permissions", ignore = true)
    RoleEntity toRoleEntity(RoleDto roleDto);
}
