package co.net.tps.core.auth.domain.service.impl;

import co.net.tps.core.auth.domain.dto.UserDto;
import co.net.tps.core.auth.domain.dto.requestbody.UserRequestBody;
import co.net.tps.core.auth.domain.enums.AuthParams;
import co.net.tps.core.auth.domain.repository.IUserRepository;
import co.net.tps.core.auth.domain.service.IPasswordRecovery;
import co.net.tps.core.auth.domain.service.IUserService;
import co.net.tps.core.exception.HttpNotFoundException;
import co.net.tps.core.parameter.domain.service.IParameterService;
import co.net.tps.core.util.CustomPage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class UserService implements IUserService {
    private final IUserRepository userRepository;
    private final IPasswordRecovery passwordRecovery;
    private final IParameterService parameterService;

    public UserService(IUserRepository iUserRepository, IPasswordRecovery passwordRecovery, IParameterService parameterService) {
        this.userRepository = iUserRepository;
        this.passwordRecovery = passwordRecovery;
        this.parameterService = parameterService;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDto findTopByUsername(String username) {
        return userRepository.findTopByUsername(username)
                .orElseThrow(() -> new HttpNotFoundException("Usuario no encontrado"));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateCurrentToken(String username, String token) {
        userRepository.updateCurrentToken(username,token);
    }

    @Override
    public CustomPage<UserDto> findAll(Pageable pageable) {
        Page<UserDto> listUser = userRepository.findAll(pageable);
        return new CustomPage<>(listUser.getContent(), pageable, listUser.getTotalElements(),null);
    }

    @Override
    public UserDto findById(int userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new HttpNotFoundException("Usuario no encontrado"));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDto create(UserRequestBody requestBody) {
        UserDto userDto = new UserDto();
        userDto.fillFromUserRequestBody(requestBody);
        userDto.setPassword("");
        userDto.setSingleSession(true);
        userDto.setChangePassword(true);

        userDto = this.save(userDto);

        int passwordLength = parameterService.getIntegerValueByKey(AuthParams.AUTH_PASSWORD_LENGTH);
        long expirationMinutes = parameterService.getIntegerValueByKey(AuthParams.AUTH_PASSWORD_EXPMIN);
        this.passwordRecovery.createPasswordRecovery(userDto.getId(), passwordLength, expirationMinutes);

        return userDto;
    }

    @Override
    public UserDto save(UserDto userDto) {
        return userRepository.save(userDto);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserDto deletedById(int userId) {
        UserDto user = userRepository.findById(userId).
                orElseThrow(() -> new HttpNotFoundException("Usuario no encontrado"));
        user.setActive(false);
        return userRepository.save(user);
    }

}
