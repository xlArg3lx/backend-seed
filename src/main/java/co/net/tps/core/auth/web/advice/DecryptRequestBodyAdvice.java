package co.net.tps.core.auth.web.advice;

import co.net.tps.core.auth.domain.util.CryptoUtil;
import co.net.tps.core.auth.web.annotation.DecryptRequestBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Base64;

@Slf4j
@RestControllerAdvice
public class DecryptRequestBodyAdvice extends RequestBodyAdviceAdapter {

    private final ObjectMapper objectMapper;
    private final CryptoUtil cryptoUtil;

    public DecryptRequestBodyAdvice(ObjectMapper objectMapper, CryptoUtil cryptoUtil) {
        this.objectMapper = objectMapper;
        this.cryptoUtil = cryptoUtil;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return methodParameter.hasMethodAnnotation(DecryptRequestBody.class);
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) throws IOException {

        try (InputStream inputStream = inputMessage.getBody()) {
            byte[] body = StreamUtils.copyToByteArray(inputStream);
            DataObjectDto requestBody = objectMapper.readValue(body, DataObjectDto.class);
            String decryptBody = cryptoUtil.decrypt(requestBody.getData());
            return new DecodeHttpInputMessage(inputMessage.getHeaders(),new ByteArrayInputStream(decryptBody.getBytes()));
        }
    }
}
