package co.net.tps.core.auth.domain.service.impl;

import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.domain.repository.IActionLogRepository;
import co.net.tps.core.auth.domain.service.IActionLogService;
import org.springframework.stereotype.Service;

@Service
public class ActionLogService  implements IActionLogService {

    private final IActionLogRepository actionLogRepository;

    public ActionLogService(IActionLogRepository actionLogRepository) {
        this.actionLogRepository = actionLogRepository;
    }

    @Override
    public void save(ActionLogDto actionLogDto) {
        actionLogRepository.save(actionLogDto);
    }
}
