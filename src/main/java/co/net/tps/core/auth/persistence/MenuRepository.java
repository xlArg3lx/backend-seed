package co.net.tps.core.auth.persistence;

import co.net.tps.core.auth.domain.dto.MenuDto;
import co.net.tps.core.auth.domain.dto.MenuItemDto;
import co.net.tps.core.auth.domain.repository.IMenuRepository;
import co.net.tps.core.auth.persistence.crudrepository.MenuCrudRepository;
import co.net.tps.core.auth.persistence.entity.MenuEntity;
import co.net.tps.core.auth.persistence.mapper.MenuMapper;
import co.net.tps.core.auth.persistence.projection.MenuItemProjection;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public class MenuRepository implements IMenuRepository {

    private final MenuCrudRepository menuCrudRepository;
    private final MenuMapper menuMapper;

    public MenuRepository(MenuCrudRepository menuCrudRepository, MenuMapper menuMapper) {
        this.menuCrudRepository = menuCrudRepository;
        this.menuMapper = menuMapper;
    }

    @Override
    public List<MenuItemDto> findByRoleId(int roleId) {
        List<MenuItemProjection> entities = menuCrudRepository.findMenuByRoleId(roleId);
        return menuMapper.toListMenuItemDto(entities);
    }

    @Override
    @Transactional
    public MenuDto save(MenuDto menuItemDto) {
        MenuEntity entity = menuMapper.toMenuEntity(menuItemDto);
        return menuMapper.toMenuDto(menuCrudRepository.save(entity));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MenuDto> findByMenuId(int menuId) {
        return menuCrudRepository.findById(menuId)
                .map(menuMapper::toMenuDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MenuDto> findAll() {
        List<MenuEntity> entities = (List<MenuEntity>) menuCrudRepository.findAll();
        return menuMapper.toListMenuDto(entities);
    }

    @Override
    @Transactional
    public void deletedById(int menuId) {
        menuCrudRepository.deleteById(menuId);
    }
}
