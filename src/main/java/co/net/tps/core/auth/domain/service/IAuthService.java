package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.ChangePasswordDto;
import co.net.tps.core.auth.domain.dto.requestbody.AuthRefreshRequestBody;
import co.net.tps.core.auth.domain.dto.requestbody.AuthRequestBody;
import co.net.tps.core.auth.domain.dto.responsebody.AuthRefreshTokenResponseBody;
import co.net.tps.core.auth.domain.dto.responsebody.AuthResponseBody;

public interface IAuthService {
    AuthResponseBody login(AuthRequestBody authRequestBody, String ipRemote);

    AuthRefreshTokenResponseBody refreshToken(AuthRefreshRequestBody authRefreshRequestBody);

    void changePassword(ChangePasswordDto changePasswordDto, String username);
}
