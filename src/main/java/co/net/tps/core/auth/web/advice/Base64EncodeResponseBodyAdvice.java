package co.net.tps.core.auth.web.advice;

import co.net.tps.core.auth.web.annotation.Base64EncodedBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@RestControllerAdvice
public class Base64EncodeResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    private final ObjectMapper objectMapper;

    public Base64EncodeResponseBodyAdvice(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return returnType.hasMethodAnnotation(Base64EncodedBody.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        try {
            String jsonBody = objectMapper.writeValueAsString(body);

            byte[] decodedBody = Base64.getEncoder().encode(jsonBody.getBytes(StandardCharsets.UTF_8));
            String data = new String(decodedBody, StandardCharsets.UTF_8);

            // log.info("response: {}", data);

            return new DataObjectDto(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
