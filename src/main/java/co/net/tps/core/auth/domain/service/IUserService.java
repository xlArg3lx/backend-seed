package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.PermissionDto;
import co.net.tps.core.auth.domain.dto.UserDto;
import co.net.tps.core.auth.domain.dto.requestbody.UserRequestBody;
import co.net.tps.core.util.CustomPage;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface IUserService {
    UserDto findTopByUsername(String username);
    void updateCurrentToken(String username, String token);
    CustomPage<UserDto> findAll(Pageable pageable);
    UserDto findById(int userId);
    UserDto create(UserRequestBody requestBody);
    UserDto save(UserDto userDto);
    UserDto deletedById(int userId);
}
