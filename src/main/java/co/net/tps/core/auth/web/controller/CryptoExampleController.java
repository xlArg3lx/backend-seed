package co.net.tps.core.auth.web.controller;

import co.net.tps.core.auth.domain.util.CryptoUtil;
import co.net.tps.core.auth.web.advice.DataObjectDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/crypto")
public class CryptoExampleController {

    private final ObjectMapper objectMapper;
    private final CryptoUtil cryptoUtil;

    public CryptoExampleController(ObjectMapper objectMapper, CryptoUtil cryptoUtil) {
        this.objectMapper = objectMapper;
        this.cryptoUtil = cryptoUtil;
    }

    @PostMapping(value = "/encrypt")
    public ResponseEntity<DataObjectDto> check(@RequestBody Object body) throws JsonProcessingException {
        String jsonBody = objectMapper.writeValueAsString(body);
        String data = cryptoUtil.encrypt(jsonBody);
        return ResponseEntity.ok(new DataObjectDto(data));
    }

    @PostMapping(value = "/decrypt")
    public ResponseEntity<Object> check(@RequestBody DataObjectDto dataObjectDto) throws JsonProcessingException {
        String data = cryptoUtil.decrypt(dataObjectDto.getData());
        Object jsonBody = objectMapper.readValue(data, Object.class);
        return ResponseEntity.ok(jsonBody);
    }
}
