package co.net.tps.core.auth.web.advice;

import co.net.tps.core.auth.domain.util.CryptoUtil;
import co.net.tps.core.auth.web.annotation.EncryptResponseBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@Slf4j
@RestControllerAdvice
public class EncryptResponseBodyAdvice implements ResponseBodyAdvice<Object> {

    private final ObjectMapper objectMapper;
    private final CryptoUtil cryptoUtil;

    public EncryptResponseBodyAdvice(ObjectMapper objectMapper, CryptoUtil cryptoUtil) {
        this.objectMapper = objectMapper;
        this.cryptoUtil = cryptoUtil;
    }

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return returnType.hasMethodAnnotation(EncryptResponseBody.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  ServerHttpRequest request, ServerHttpResponse response) {
        try {
            String jsonBody = objectMapper.writeValueAsString(body);
            String data = cryptoUtil.encrypt(jsonBody);
            return new DataObjectDto(data);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
