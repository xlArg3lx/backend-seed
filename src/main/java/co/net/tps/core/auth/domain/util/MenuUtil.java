package co.net.tps.core.auth.domain.util;

import co.net.tps.core.auth.domain.dto.MenuBranchDto;
import co.net.tps.core.auth.domain.dto.MenuItemDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MenuUtil {

    public List<MenuBranchDto> buildNestedMenu(List<MenuItemDto> menu, int parentId) {
        List<MenuBranchDto> branches = new ArrayList<>();
        for (MenuItemDto item : menu) {
            if (item.getParentId() != parentId)
                continue;
            List<MenuBranchDto> children = buildNestedMenu(menu, item.getId());

            MenuBranchDto branch = new MenuBranchDto(item);
            if ((!children.isEmpty()))
                branch.setChildren(children);
            branches.add(branch);
        }
        return branches;
    }

}
