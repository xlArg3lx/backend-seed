package co.net.tps.core.auth.domain.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Slf4j
@Component
public class CryptoUtil {

    @Value("${auth.crypto.secret}")
    private String secretKey;

    @Value("${auth.crypto.aes-algorithm}")
    private String aesAlgorithm;

    private final String AES = "AES";


    public String encrypt(String src)  {
        try {
            SecureRandom secureRandom = new SecureRandom();
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), AES);
            Cipher cipher = Cipher.getInstance(aesAlgorithm);

            byte[] iv = new byte[16];
            secureRandom.nextBytes(iv);

            IvParameterSpec ivParameterSpec = new IvParameterSpec(iv);

            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);

            byte[] encrypted = cipher.doFinal(src.getBytes(StandardCharsets.UTF_8));
            String encryptedData = Base64.getEncoder().encodeToString(encrypted);
            String encryptedIv = Base64.getEncoder().encodeToString(iv);
            return encryptedData + ":" + encryptedIv;
        } catch (NoSuchPaddingException
                 | NoSuchAlgorithmException
                 | InvalidAlgorithmParameterException
                 | InvalidKeyException
                 | IllegalBlockSizeException
                 | BadPaddingException e) {
            log.error("Error encriptando respuesta: {}", e.getMessage());
            return "";
        }
    }

    public String decrypt(String src) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(StandardCharsets.UTF_8), AES);
            Cipher cipher = Cipher.getInstance(aesAlgorithm);

            String[] data = src.split(":");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(Base64.getDecoder().decode(data[1]));

            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, ivParameterSpec);

            byte[] originalSrc = Base64.getDecoder().decode(data[0]);
            byte[] original = cipher.doFinal(originalSrc);

            return new String(original, StandardCharsets.UTF_8);
        } catch (Exception e) {
            return "";
        }
    }
}
