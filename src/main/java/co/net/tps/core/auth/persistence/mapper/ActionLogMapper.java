package co.net.tps.core.auth.persistence.mapper;

import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.persistence.entity.ActionLogEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ActionLogMapper {

    ActionLogDto toActionLogDto(ActionLogEntity actionLogEntity);

    @Mapping(target = "afterValue", ignore = true)
    @Mapping(target = "beforeValue", ignore = true)
    ActionLogEntity toActionLogEntity(ActionLogDto actionLogDto);
}
