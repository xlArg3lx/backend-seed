package co.net.tps.core.auth.domain.service.impl;

import co.net.tps.core.auth.domain.dto.PermissionDto;
import co.net.tps.core.auth.domain.repository.IPermissionRepository;
import co.net.tps.core.auth.domain.service.IPermissionService;
import co.net.tps.core.exception.HttpNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService implements IPermissionService {

    private final IPermissionRepository permissionRepository;

    public PermissionService(IPermissionRepository permissionRepository) {
        this.permissionRepository = permissionRepository;
    }


    @Override
    public List<PermissionDto> findAll() {
        return permissionRepository.findAll();
    }

    @Override
    public PermissionDto findById(int permissionId) {
        return permissionRepository.findById(permissionId)
                .orElseThrow(() -> new HttpNotFoundException("Permiso no encontrado"));
    }

    @Override
    public PermissionDto save(PermissionDto permissionDto) {
        return permissionRepository.save(permissionDto);
    }

    @Override
    public void deletedById(int permissionId) {
        permissionRepository.deletedById(permissionId);
    }
}
