package co.net.tps.core.auth.persistence;

import co.net.tps.core.auth.domain.dto.PermissionDto;
import co.net.tps.core.auth.domain.repository.IPermissionRepository;
import co.net.tps.core.auth.persistence.crudrepository.PermissionCrudRepository;
import co.net.tps.core.auth.persistence.entity.PermissionEntity;
import co.net.tps.core.auth.persistence.mapper.PermissionMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PermissionRepository implements IPermissionRepository {
    private final PermissionCrudRepository permissionCrudRepository;
    private final PermissionMapper permissionMapper;

    public PermissionRepository(PermissionCrudRepository permissionCrudRepository, PermissionMapper permissionMapper) {
        this.permissionCrudRepository = permissionCrudRepository;
        this.permissionMapper = permissionMapper;
    }

    @Override
    public List<PermissionDto> findAll() {
        List<PermissionEntity> entities = (List<PermissionEntity>) permissionCrudRepository.findAll();
        return permissionMapper.toListPermissionDto(entities);
    }

    @Override
    public Optional<PermissionDto> findById(int permissionId) {
        return permissionCrudRepository.findById(permissionId)
                .map(permissionMapper::toPermissionDto);
    }

    @Override
    public PermissionDto save(PermissionDto permissionDto) {
        PermissionEntity entity = permissionMapper.toPermissionEntity(permissionDto);
        return permissionMapper.toPermissionDto(permissionCrudRepository.save(entity));
    }

    @Override
    public void deletedById(int permissionId) {
        permissionCrudRepository.deleteById(permissionId);
    }
}
