package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.ChangePasswordDto;
import co.net.tps.core.auth.domain.dto.MenuBranchDto;
import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;
import co.net.tps.core.auth.domain.dto.RoleDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

public interface IAuthUserDetailService extends UserDetailsService {
    @Transactional(readOnly = true)
    RoleDto getRole(int roleId) throws UsernameNotFoundException;

    @Transactional(readOnly = true)
    Collection<String> getPermissionsByRoleId(int roleId);

    @Transactional(readOnly = true)
    Collection<MenuBranchDto> getMenuTreeByRoleId(int roleId);

    @Transactional(readOnly = true)
    Optional<PasswordRecoveryDto> findPasswordRecovery(int userId);

    void updateCurrentToken(String username, String token);
    void changePassword(String username, String newPasswordEncode);
    void savePasswordRecovery(PasswordRecoveryDto passwordRecovery);

}
