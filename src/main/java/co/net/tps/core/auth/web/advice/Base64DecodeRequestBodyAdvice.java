package co.net.tps.core.auth.web.advice;

import co.net.tps.core.auth.web.annotation.Base64EncodedBody;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Base64;

@Slf4j
@RestControllerAdvice
public class Base64DecodeRequestBodyAdvice extends RequestBodyAdviceAdapter {

    private final ObjectMapper objectMapper;

    public Base64DecodeRequestBodyAdvice(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public boolean supports(MethodParameter methodParameter, Type targetType, Class<? extends HttpMessageConverter<?>> converterType) {
        return methodParameter.hasMethodAnnotation(Base64EncodedBody.class);
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) throws IOException {

        try (InputStream inputStream = inputMessage.getBody()) {

            // Read request body
            byte[] body = StreamUtils.copyToByteArray(inputStream);
            DataObjectDto requestBody = objectMapper.readValue(body, DataObjectDto.class);
            // log.info("raw: {}", new String(body));


            // Base64 Decode
            byte[] decodedBody = Base64.getDecoder().decode(requestBody.getData());
            // log.info("decode: {}", new String(decodedBody, StandardCharsets.UTF_8));

            // Return the decoded body
            return new DecodeHttpInputMessage(inputMessage.getHeaders(),new ByteArrayInputStream(decodedBody));
        }
    }
}
