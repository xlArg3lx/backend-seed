package co.net.tps.core.auth.domain.service.impl;

import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;
import co.net.tps.core.auth.domain.repository.IPasswordRecoveryRepository;
import co.net.tps.core.auth.domain.service.IPasswordRecovery;
import co.net.tps.core.auth.domain.util.PasswordUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Slf4j
@Service
public class PasswordRecovery implements IPasswordRecovery {

    private final IPasswordRecoveryRepository passwordRecoveryRepository;
    private final PasswordUtil passwordUtil;

    public PasswordRecovery(IPasswordRecoveryRepository passwordRecoveryRepository, PasswordUtil passwordUtil) {
        this.passwordRecoveryRepository = passwordRecoveryRepository;
        this.passwordUtil = passwordUtil;
    }

    @Override
    public void createPasswordRecovery(int userId, int passwordLength, long expirationMinutes) {
        String password = passwordUtil.generatePassword(passwordLength);
        log.info("Password Recovery Created: {}", password);

        PasswordRecoveryDto passwordRecoveryDto = new PasswordRecoveryDto();

        passwordRecoveryDto.setUserId(userId);
        passwordRecoveryDto.setRecovered(false);
        passwordRecoveryDto.setPassword(passwordUtil.encode(password));
        passwordRecoveryDto.setCreatedAt(LocalDateTime.now());
        passwordRecoveryDto.setExpiration(LocalDateTime.now().plusMinutes(expirationMinutes));

        this.save(passwordRecoveryDto);
    }

    @Override
    public PasswordRecoveryDto save(PasswordRecoveryDto passwordRecovery) {
        return passwordRecoveryRepository.save(passwordRecovery);
    }
}
