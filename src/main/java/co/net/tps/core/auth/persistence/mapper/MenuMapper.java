package co.net.tps.core.auth.persistence.mapper;

import co.net.tps.core.auth.domain.dto.MenuDto;
import co.net.tps.core.auth.domain.dto.MenuItemDto;
import co.net.tps.core.auth.persistence.entity.MenuEntity;
import co.net.tps.core.auth.persistence.projection.MenuItemProjection;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MenuMapper {

	MenuItemDto toMenuItemDto(MenuItemProjection entity);
	List<MenuItemDto> toListMenuItemDto(List<MenuItemProjection> entity);

	@Mapping(target = "children", ignore = true)
	MenuDto toMenuDto(MenuEntity entity);
	MenuEntity toMenuEntity(MenuDto dto);

	List<MenuDto> toListMenuDto(List<MenuEntity> entity);
	List<MenuEntity> toListMenuEntity(List<MenuDto> dto);
}
