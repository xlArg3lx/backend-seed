package co.net.tps.core.auth.persistence.mapper;

import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;
import co.net.tps.core.auth.persistence.entity.PasswordRecoveryEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PasswordRecoveryMapper {
	PasswordRecoveryEntity toPasswordRecoveryEntity(PasswordRecoveryDto dto);
	PasswordRecoveryDto toPasswordRecoveryDto(PasswordRecoveryEntity entity);
}
