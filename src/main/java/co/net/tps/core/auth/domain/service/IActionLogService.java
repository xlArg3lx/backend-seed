package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.ActionLogDto;

public interface IActionLogService {
    void save(ActionLogDto actionLogDto);
}
