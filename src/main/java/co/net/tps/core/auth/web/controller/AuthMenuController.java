package co.net.tps.core.auth.web.controller;

import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.domain.dto.MenuDto;
import co.net.tps.core.auth.domain.dto.requestbody.MenuRequestBody;
import co.net.tps.core.auth.domain.enums.ActionLogEnum;
import co.net.tps.core.auth.domain.service.IMenuService;
import co.net.tps.core.util.ObjectUtils;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/auth/menu")
@SecurityRequirement(name = "bearerAuth")
public class AuthMenuController {

    private static final String MODULE = "AUTH:MENU";
    private final IMenuService menuService;

    public AuthMenuController(IMenuService menuService) {
        this.menuService = menuService;
    }

    @PreAuthorize("hasAuthority('menu:index')")
    @GetMapping("/all")
    public ResponseEntity<List<MenuDto>> index(HttpServletRequest request) {
        List<MenuDto> response = menuService.findAll();

        ActionLogDto actionLogDto = ActionLogDto.builder()
                .module(MODULE).action("READ")
                .message("Listar todos los menus.")
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDto);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('menu:index')")
    @GetMapping("/tree")
    public ResponseEntity<List<MenuDto>> tree(HttpServletRequest request) {
        List<MenuDto> response = menuService.getAllTree();

        ActionLogDto actionLogDto = ActionLogDto.builder()
                .module(MODULE).action("READ")
                .message("Listar arbol de menus.")
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDto);
        return ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('menu:show')")
    @GetMapping("/{menu_id}")
    public ResponseEntity<MenuDto> show(
            @PathVariable("menu_id")
            @Parameter(description = "ID del menú a obtener")
            Integer menuId,
            @Parameter(hidden = true)
            HttpServletRequest request) {
        MenuDto response = menuService.findById(menuId);

        ActionLogDto actionLogDto = ActionLogDto.builder()
                .module(MODULE).action("READ")
                .message("Listar menu id " + response.getId())
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDto);
        return  ResponseEntity.ok(response);
    }

    @PreAuthorize("hasAuthority('menu:store')")
    @PostMapping
    public ResponseEntity<MenuDto> store(@Valid @RequestBody MenuDto menuItemDto, HttpServletRequest request) {
        MenuDto newMenu = menuService.save(menuItemDto);

        ActionLogDto actionLogDto = ActionLogDto.builder()
                .module(MODULE).action("CREATE")
                .message("Creando menu con id " + newMenu.getId())
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(newMenu);
    }

    @PreAuthorize("hasAuthority('menu:update')")
    @PutMapping("/{menu_id}")
    public ResponseEntity<MenuDto> update
            (@PathVariable("menu_id")
             @Parameter(description = "ID del menú a actualizar")
             Integer menuId,
             @RequestBody @Valid MenuRequestBody requestBody,
             HttpServletRequest request) {

        MenuDto menuDto = menuService.findById(menuId);
        MenuDto beforeValue = ObjectUtils.clone(menuDto);

        menuDto.fillFromMenuRequestBody(requestBody);
        MenuDto afterValue = menuService.save(menuDto);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("UPDATE")
                .message("Editando menu con id " + afterValue.getId())
                .beforeValue(beforeValue)
                .afterValue(afterValue)
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.ok(afterValue);
    }

    @PreAuthorize("hasAuthority('menu:delete')")
    public ResponseEntity<MenuDto> delete
            (@PathVariable("menu_id")
             @Parameter(description = "ID del menú a eliminar")
             Integer menuId,
             HttpServletRequest request) {
        MenuDto beforeValue = menuService.findById(menuId);
        menuService.deletedById(menuId);

        ActionLogDto actionLogDTO = ActionLogDto.builder()
                .module(MODULE).action("DELETE")
                .message("Eliminar menú con id " + menuId)
                .beforeValue(beforeValue)
                .build();

        request.setAttribute(ActionLogEnum.ATTRIBUTE_NAME.toString(), actionLogDTO);
        return ResponseEntity.noContent().build();
    }
}
