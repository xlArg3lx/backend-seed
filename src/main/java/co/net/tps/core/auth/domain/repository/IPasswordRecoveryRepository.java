package co.net.tps.core.auth.domain.repository;

import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;

import java.util.Optional;

public interface IPasswordRecoveryRepository {
	PasswordRecoveryDto save(PasswordRecoveryDto passwordRecoveryDto);
	Optional<PasswordRecoveryDto> findPasswordRecoveryByUserId(int userId);
}
