package co.net.tps.core.auth.persistence;

import co.net.tps.core.auth.domain.dto.ActionLogDto;
import co.net.tps.core.auth.domain.repository.IActionLogRepository;
import co.net.tps.core.auth.persistence.crudrepository.ActionLogCrudRepository;
import co.net.tps.core.auth.persistence.entity.ActionLogEntity;
import co.net.tps.core.auth.persistence.mapper.ActionLogMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class ActionLogRepository implements IActionLogRepository {

    private final ActionLogCrudRepository actionLogCrudRepository;
    private final ActionLogMapper actionLogMapper;
    private final ObjectMapper objectMapper;

    public ActionLogRepository(ActionLogCrudRepository actionLogCrudRepository, ActionLogMapper actionLogMapper, ObjectMapper objectMapper) {
        this.actionLogCrudRepository = actionLogCrudRepository;
        this.actionLogMapper = actionLogMapper;
        this.objectMapper = objectMapper;
    }

    @Override
    public void save(ActionLogDto actionLogDto) {
        ActionLogEntity entity = actionLogMapper.toActionLogEntity(actionLogDto);
        entity.setAfterValue(mapValue(actionLogDto.getAfterValue()));
        entity.setBeforeValue(mapValue(actionLogDto.getBeforeValue()));

        actionLogCrudRepository.save(entity);
    }

    private String mapValue(Object object) {
        String mappedValue = null;
        if (object != null) {
            try {
                mappedValue = objectMapper.writeValueAsString(object);
            } catch (JsonProcessingException e) {
                log.error("No fue posible convertir un valor a string para auditoría", e);
            }
        }
        return mappedValue;
    }
}
