package co.net.tps.core.auth.domain.service;

import co.net.tps.core.auth.domain.dto.PasswordRecoveryDto;

public interface IPasswordRecovery {
    void createPasswordRecovery(int userId, int passwordLength, long expirationMinutes);
    PasswordRecoveryDto save(PasswordRecoveryDto passwordRecovery);
}
