package co.net.tps.core.auth.domain.service.impl;

import co.net.tps.core.auth.domain.dto.MenuDto;
import co.net.tps.core.auth.domain.dto.tree.NodeTree;
import co.net.tps.core.auth.domain.repository.IMenuRepository;
import co.net.tps.core.auth.domain.service.IMenuService;
import co.net.tps.core.exception.HttpNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuService implements IMenuService {

    private final IMenuRepository menuRepository;

    public MenuService(IMenuRepository permissionRepository) {
        this.menuRepository = permissionRepository;
    }


    @Override
    public List<MenuDto> findAll() {
        return menuRepository.findAll();
    }

    @Override
    public List<MenuDto> getAllTree() {
        List<MenuDto> menu = menuRepository.findAll();
        return NodeTree.buildNestedNodeTree(menu);
    }

    @Override
    public MenuDto findById(int menuId) {
        return menuRepository.findByMenuId(menuId)
                .orElseThrow(() -> new HttpNotFoundException("Menú no encontrado"));
    }

    @Override
    public MenuDto save(MenuDto menuItemDto) {
        return menuRepository.save(menuItemDto);
    }

    @Override
    public void deletedById(int menuId) {
        menuRepository.deletedById(menuId);
    }
}
