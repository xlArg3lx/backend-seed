package co.net.tps.core.auth.persistence.crudrepository;

import co.net.tps.core.auth.persistence.entity.ActionLogEntity;
import org.springframework.data.repository.CrudRepository;

public interface ActionLogCrudRepository extends CrudRepository<ActionLogEntity, Long> {
}
