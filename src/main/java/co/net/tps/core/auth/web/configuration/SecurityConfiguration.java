package co.net.tps.core.auth.web.configuration;

import co.net.tps.core.auth.domain.service.impl.AuthUserDetailService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableMethodSecurity
public class SecurityConfiguration {

    private final JwtFilterRequest jwtFilterRequest;
    private final UnauthorizedEntryPoint unauthorizedHandler;

    private final String[] authPathWhiteList = new String[] {
            "/monitor**",
            "/auth/login",
            "/auth/refresh-token",
            "/auth/recovery-password",
            "/v3/api-docs**",
            "/swagger-ui/**",
            "/swagger-ui.html",
            "/swagger-docs/**",
            "/crypto/encrypt",
            "/crypto/decrypt"
    };

    public SecurityConfiguration(JwtFilterRequest jwtFilterRequest, UnauthorizedEntryPoint unauthorizedHandler) {
        this.jwtFilterRequest = jwtFilterRequest;
        this.unauthorizedHandler = unauthorizedHandler;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        return httpSecurity
                .cors(AbstractHttpConfigurer::disable)
                .csrf(AbstractHttpConfigurer::disable)
                .authorizeHttpRequests(a -> a
                        .requestMatchers(authPathWhiteList).permitAll()
                        .anyRequest().authenticated())
                .exceptionHandling(e -> e.authenticationEntryPoint(unauthorizedHandler))
                .sessionManagement(s -> s.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(jwtFilterRequest, UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    public UserDetailsService userDetailService() {
        return new AuthUserDetailService();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
