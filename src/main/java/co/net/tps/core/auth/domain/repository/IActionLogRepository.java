package co.net.tps.core.auth.domain.repository;

import co.net.tps.core.auth.domain.dto.ActionLogDto;

public interface IActionLogRepository {
    void save(ActionLogDto actionLogDto);
}
