package co.net.tps.core.monitor.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonitorStatusDto {

    private String status;
    private String version;
    private boolean database;
    private String time;

}
