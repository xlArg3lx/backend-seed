package co.net.tps.core.monitor.domain.service.impl;

import co.net.tps.core.monitor.domain.dto.MonitorStatusDto;
import co.net.tps.core.monitor.domain.service.IMonitorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class MonitorService implements IMonitorService {

    @Autowired
    private DataSource dataSource;

    @Value("${info.build.version}")
    private String version;

    @Override
    public MonitorStatusDto getStatus() {
        MonitorStatusDto monitorStatus = new MonitorStatusDto();

        monitorStatus.setVersion(version);
        monitorStatus.setTime(LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME));
        monitorStatus.setDatabase(checkConnectionDatabase());
        monitorStatus.setStatus("OK");

        return monitorStatus;
    }

    private boolean checkConnectionDatabase() {
        try (Connection conn = DataSourceUtils.getConnection(dataSource)) {
            return conn.isValid(5);
        } catch (Exception e) {
            log.error("Fallo test de conexión a la Base de datos. " + e.getMessage());
            return false;
        }
    }
}
