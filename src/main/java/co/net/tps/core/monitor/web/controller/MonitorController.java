package co.net.tps.core.monitor.web.controller;

import co.net.tps.core.monitor.domain.dto.MonitorStatusDto;
import co.net.tps.core.monitor.domain.service.IMonitorService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/monitor")
public class MonitorController {

    private final IMonitorService monitorService;

    public MonitorController(IMonitorService monitorService) {
        this.monitorService = monitorService;
    }

    @GetMapping
    public ResponseEntity<MonitorStatusDto> index() {
        MonitorStatusDto response = monitorService.getStatus();
        return ResponseEntity.ok(response);
    }

    @GetMapping("/ping")
    public ResponseEntity<String> ping() {
        return ResponseEntity.ok("pong!!");
    }

}
