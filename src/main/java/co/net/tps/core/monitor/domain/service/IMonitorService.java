package co.net.tps.core.monitor.domain.service;

import co.net.tps.core.monitor.domain.dto.MonitorStatusDto;

public interface IMonitorService {
    MonitorStatusDto getStatus();
}
