package co.net.tps.core.exception;

import org.springframework.http.HttpStatus;

public class HttpInternalServerErrorException extends HttpGenericException {

    public HttpInternalServerErrorException(String statusText) {
        super(HttpStatus.INTERNAL_SERVER_ERROR, statusText);
    }
}
