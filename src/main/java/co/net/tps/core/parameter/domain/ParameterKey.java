package co.net.tps.core.parameter.domain;

public interface ParameterKey {
    String getValue();
}
