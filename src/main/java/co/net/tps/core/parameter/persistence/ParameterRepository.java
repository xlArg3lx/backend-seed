package co.net.tps.core.parameter.persistence;

import co.net.tps.core.parameter.domain.repository.IParameterRepository;
import co.net.tps.core.parameter.persistence.crudrepository.ParameterCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class ParameterRepository implements IParameterRepository {

    private final ParameterCrudRepository parameterCrudRepository;

    public ParameterRepository(ParameterCrudRepository parameterCrudRepository) {
        this.parameterCrudRepository = parameterCrudRepository;
    }

    @Override
    public String getValueByKey(String key) {
        Optional<String> optionalValue = parameterCrudRepository.getValueByKey(key);
        return optionalValue.map(String::valueOf).orElse(null);
    }


}
