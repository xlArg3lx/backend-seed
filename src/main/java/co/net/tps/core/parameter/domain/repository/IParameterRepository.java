package co.net.tps.core.parameter.domain.repository;

public interface IParameterRepository {
    String getValueByKey(String key);
}
