package co.net.tps.core.parameter.domain.service;

import co.net.tps.core.parameter.domain.ParameterKey;

public interface IParameterService {
    String getValueByKey(ParameterKey parameterKey);
    String getValueByKeyOrDefault(ParameterKey parameterKey, String defaultValue);
    Integer getIntegerValueByKey(ParameterKey parameterKey);
    boolean getBooleanValueByKey(ParameterKey parameterKey);
}
