package co.net.tps.core.parameter.domain.service.impl;

import co.net.tps.core.parameter.domain.ParameterKey;
import co.net.tps.core.parameter.domain.repository.IParameterRepository;
import co.net.tps.core.parameter.domain.service.IParameterService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ParameterService implements IParameterService {

    private final IParameterRepository parameterRepository;

    private static final String YES = "S";

    public ParameterService(IParameterRepository parameterRepository) {
        this.parameterRepository = parameterRepository;
    }

    @Override
    public String getValueByKey(ParameterKey parameterKey) {
        return parameterRepository.getValueByKey(parameterKey.getValue());
    }

    @Override
    public String getValueByKeyOrDefault(ParameterKey parameterKey, String defaultValue) {
        String value = this.getValueByKey(parameterKey);
        return Objects.nonNull(value) ? value : defaultValue;
    }

    @Override
    public Integer getIntegerValueByKey(ParameterKey parameterKey) {
        try {
            return Integer.valueOf(getValueByKeyOrDefault(parameterKey, null));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    @Override
    public boolean getBooleanValueByKey(ParameterKey parameterKey) {
        return YES.equals(getValueByKeyOrDefault(parameterKey, "N"));
    }

}
