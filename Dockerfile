# Etapa de construcción
FROM maven:3.9.6-eclipse-temurin-21-alpine AS build
WORKDIR /app

COPY pom.xml .
RUN mvn dependency:go-offline

# Copia el código fuente y compila el proyecto
COPY src ./src
RUN mvn package -DskipTests

# Etapa de producción
FROM eclipse-temurin:21.0.2_13-jdk-alpine AS production
WORKDIR /app
COPY --from=build /app/target/*.war app.war
EXPOSE 8080
CMD ["java", "-jar", "app.war"]
